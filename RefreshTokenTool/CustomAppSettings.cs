﻿using System.Configuration;

namespace RefreshTokenTool
{
    public class CustomAppSettings
    {
        public string AuthBaseUrl
        {
            get { return ConfigurationManager.AppSettings["AuthBaseUrl"]; }
            set { SetValue("AuthBaseUrl", value); }
        }

        public string ClientId
        {
            get { return ConfigurationManager.AppSettings["ClientId"]; }
            set { SetValue("ClientId", value); }
        }

        public string ClientSecret
        {
            get { return ConfigurationManager.AppSettings["ClientSecret"]; }
            set { SetValue("ClientSecret", value); }
        }

        public string RedirectUrl
        {
            get { return ConfigurationManager.AppSettings["AuthenticationRedirectUrl"]; }
            set { SetValue("AuthenticationRedirectUrl", value); }
        }

        public string Scopes
        {
            get { return ConfigurationManager.AppSettings["Scopes"]; }
            set { SetValue("Scopes", value); }
        }

        private static void SetValue(string name, string value)
        {
            var config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
            config.AppSettings.Settings[name].Value = value;
            config.Save(ConfigurationSaveMode.Modified);
            ConfigurationManager.RefreshSection("appSettings");
        }
    }
}