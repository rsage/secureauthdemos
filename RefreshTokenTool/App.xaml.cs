﻿using System;
using System.Diagnostics;
using System.Text;
using System.Windows.Threading;

namespace RefreshTokenTool
{
    public partial class App
    {
        private void App_OnDispatcherUnhandledException(object sender, DispatcherUnhandledExceptionEventArgs e)
        {
            var message = new StringBuilder();
            message.AppendFormat("{0} - Application crashed.", DateTime.Now);
            message.AppendLine();
            message.Append(e.Exception);
            Trace.TraceError(message.ToString());
        }
    }
}
