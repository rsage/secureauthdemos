﻿using System;
using System.Diagnostics;
using System.Threading.Tasks;
using System.Web;
using System.Windows;
using System.Windows.Navigation;
using Thinktecture.IdentityModel.Client;

namespace RefreshTokenTool
{
    public partial class MainWindow
    {
        private readonly MainModel _model = new MainModel();

        public MainWindow()
        {
            InitializeComponent();

            DataContext = _model;
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            LogInformation("Main window loaded.");
        }

        private void LoginButton_Click(object sender, RoutedEventArgs e)
        {
            LogInformation("Login. URL: {0}", _model.LoginUrl);

            //Navigate to authentication URL (step 1).
            WebBrowser.Navigate(_model.LoginUrl);
            _model.WebBrowserVisibility = Visibility.Visible;
        }

        private void LogoutButton_Click(object sender, RoutedEventArgs e)
        {
            LogInformation("Logout. URL: {0}", _model.LogoutUrl);

            WebBrowser.Navigate(_model.LogoutUrl);
            _model.WebBrowserVisibility = Visibility.Visible;
        }

        private async void WebBrowser_Navigating(object sender, NavigatingCancelEventArgs e)
        {
            //Intercept redirect request. 
            //The redirect request is the requires to Redirect URL, which was provider is first step requires.
            if (_model.IsRedirectUrl(e.Uri))
            {
                _model.ProgressVisibility = Visibility.Visible;

                LogInformation("Redirect response received. URL: {0}", e.Uri);

                var accessCode = GetAccessCodeFromUrl(e.Uri);

                LogInformation("One time security code: {0}", accessCode);

                //Request refresh token from authentication service.
                var refreshToken = await GetRefreshTokenAsync(accessCode);

                _model.RefreshToken = refreshToken;
                _model.AccessCode = accessCode;
                _model.ProgressVisibility = Visibility.Hidden;
                _model.WebBrowserVisibility = Visibility.Collapsed;
            }
        }

        /// <summary>
        /// Requests refresh token from security service. (Step 2)
        /// It is simple REST-request to token endpoint. The response is json-object.
        /// Thinktecture OpenID Client is used to make actual call and parse the response. 
        /// Libraries list for other platforms can be found here: http://openid.net/developers/libraries/
        /// </summary>
        /// <param name="securityCode"></param>
        /// <returns></returns>
        private async Task<string> GetRefreshTokenAsync(string securityCode)
        {
            LogInformation("Refresh token receiving...");

            var oidcClient = new OAuth2Client(new Uri(_model.TokenEndpoint, UriKind.Absolute), _model.ClientId, _model.ClientSecret, OAuth2Client.ClientAuthenticationStyle.PostValues);
            var response = await oidcClient.RequestAuthorizationCodeAsync(securityCode, _model.RedirectUrl);

            if (response.IsError)
            {
                LogInformation("Refresh token receiving failed. {0}", response.Error);
            }
            else
            {
                LogInformation("Refresh token received successfully.");
            }

            return response.RefreshToken;
        }

        /// <summary>
        /// Extract one time security code from URL
        /// One time security code is passes as redirect request query "code" parameter value.
        /// </summary>
        /// <param name="url"></param>
        /// <returns></returns>
        private static string GetAccessCodeFromUrl(Uri url)
        {
            var queryParams = HttpUtility.ParseQueryString(url.Query);

            return queryParams["code"];
        }

        private static void LogInformation(string messageFormat, params object[] args)
        {
            var message = string.Format(messageFormat, args);
            Trace.TraceInformation("{0}: {1}", DateTime.Now, message);
        }

        private void CopyRefreshTokenButton_Click(object sender, RoutedEventArgs e)
        {
            Clipboard.SetText(_model.RefreshToken);
        }

        private void CopyCodeButton_Click(object sender, RoutedEventArgs e)
        {
            Clipboard.SetText(_model.AccessCode);
        }
    }
}
