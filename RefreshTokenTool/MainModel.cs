﻿using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Web;
using System.Windows;
using RefreshTokenTool.Annotations;

namespace RefreshTokenTool
{
    public class MainModel: INotifyPropertyChanged
    {
        private readonly CustomAppSettings _settings = new CustomAppSettings();
        private string _state = Guid.NewGuid().ToString();
        private Visibility _webBrowserVisibility = Visibility.Collapsed;
        private Visibility _progressVisibility = Visibility.Hidden;
        private string _refreshToken;
        private string _accessCode;

        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            var handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }

        public string AuthBaseUrl
        {
            get { return _settings.AuthBaseUrl; }
            set
            {
                _settings.AuthBaseUrl = value;
                OnPropertyChanged();
                OnPropertyChanged("LoginUrl");
                OnPropertyChanged("LogoutUrl");
                OnPropertyChanged("TokenEndpoint");
                OnPropertyChanged("AuthEndpoint");
            }
        }

        public string ClientId
        {
            get { return _settings.ClientId; }
            set
            {
                _settings.ClientId = value;
                OnPropertyChanged();
                OnPropertyChanged("LoginUrl");
                OnPropertyChanged("LogoutUrl");

            }
        }

        public string ClientSecret
        {
            get { return _settings.ClientSecret; }
            set
            {
                _settings.ClientSecret = value;
                OnPropertyChanged();
            }
        }

        public string RedirectUrl
        {
            get { return _settings.RedirectUrl; }
            set
            {
                _settings.RedirectUrl = value;
                OnPropertyChanged();
                OnPropertyChanged("LoginUrl");
                OnPropertyChanged("LogoutUrl");

            }
        }

        public string Scopes
        {
            get { return _settings.Scopes; }
            set
            {
                _settings.Scopes = value;
                OnPropertyChanged();
                OnPropertyChanged("LoginUrl");
                OnPropertyChanged("LogoutUrl");

            }
        }

        public string State
        {
            get { return _state; }
            set
            {
                _state = value;
                OnPropertyChanged();
                OnPropertyChanged("LoginUrl");
                OnPropertyChanged("LogoutUrl");

            }
        }

        public string AuthEndpoint
        {
            get { return Combine(AuthBaseUrl, "authorized/oidcauthorize.aspx"); }
        }

        public string TokenEndpoint
        {
            get { return Combine(AuthBaseUrl, "oidctoken.aspx"); }
        }

        public string LoginUrl
        {
            get
            {
                var query = HttpUtility.ParseQueryString(string.Empty);
                query["client_id"] = ClientId;
                query["redirect_uri"] = RedirectUrl;
                query["response_mode"] = "query";
                query["response_type"] = "code";
                query["scope"] = Scopes;
                query["state"] = State;

                var url = AuthEndpoint + "?" + query;
                return url;
            }
        }

        public string LogoutUrl
        {
            get { return Combine(AuthBaseUrl, "restart.aspx?ReturnStr=" + HttpUtility.UrlEncode(LoginUrl)); }
        }

        public Visibility WebBrowserVisibility
        {
            get { return _webBrowserVisibility; }
            set
            {
                _webBrowserVisibility = value;
                OnPropertyChanged();
            }
        }

        public Visibility ProgressVisibility
        {
            get { return _progressVisibility; }
            set
            {
                _progressVisibility = value;
                OnPropertyChanged();
            }
        }

        public string RefreshToken
        {
            get { return _refreshToken; }
            set
            {
                if (value == _refreshToken) return;
                _refreshToken = value;
                OnPropertyChanged();
            }
        }

        public string AccessCode
        {
            get { return _accessCode; }
            set
            {
                if (value == _accessCode) return;
                _accessCode = value;
                OnPropertyChanged();
            }
        }

        public bool IsRedirectUrl(Uri uri)
        {
            return uri.AbsoluteUri.StartsWith(RedirectUrl, StringComparison.CurrentCultureIgnoreCase);
        }

        private static string Combine(string baseUrl, string relative)
        {
            return baseUrl.EndsWith("/") ? baseUrl + relative : baseUrl + "/" + relative;
        }
    }
}
