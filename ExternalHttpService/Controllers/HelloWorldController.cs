﻿using System.Net.Http;
using System.Security.Claims;
using System.Web.Http;

namespace ExternalHttpService.Controllers
{
    public class HelloWorldController : ApiController
    {
        [Authorize]
        public string Get()
        {
            var user = Request.GetOwinContext().Authentication.User;
            return "Hello " + user.FindFirst(ClaimTypes.NameIdentifier).Value;
        }
    }
}
