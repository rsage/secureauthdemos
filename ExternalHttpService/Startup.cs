﻿using System.Security.Cryptography.X509Certificates;
using System.Web;
using System.Web.Http;
using ExternalHttpService;
using Microsoft.Owin;
using Microsoft.Owin.Security.Jwt;
using Owin;

[assembly: OwinStartup(typeof(Startup))]

namespace ExternalHttpService
{
    public class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            var certificate = HttpContext.Current.Server.MapPath("~/AcrConnect.cer");
            var cert = new X509Certificate2(certificate);
            app.UseJwtBearerAuthentication(new JwtBearerAuthenticationOptions
            {
                IssuerSecurityTokenProviders = new[]
                {
                    new X509CertificateSecurityTokenProvider("https://SecureAuth01VM.acr.org", cert)
                },
                AllowedAudiences = new[] { "https://SecureAuth01VM.acr.org" },
            });

            var config = new HttpConfiguration();
            WebApiConfig.Register(config);
            app.UseWebApi(config);
        }
    }
}