﻿using System.Web;
using System.Web.Mvc;

namespace AcrIdDemo.Controllers
{
    public class HomeController : Controller
    {
        // GET: Home
        public ActionResult Index()
        {
            var user = Request.GetOwinContext().Authentication.User;
            return View();
        }
    }
}