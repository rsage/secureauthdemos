﻿using System.Web;
using System.Web.Mvc;
using Microsoft.Owin.Security;

namespace AcrIdDemo.Controllers
{
    public class AuthenticationController : Controller
    {
        public ActionResult AcrConnectLogin()
        {
            return new ChallengeResult("ACR ID", "/Home");
        }
    }

    internal class ChallengeResult : HttpUnauthorizedResult
    {
        public ChallengeResult(string provider, string redirectUri)
        {
            LoginProvider = provider;
            RedirectUri = redirectUri;
        }

        public string LoginProvider { get; set; }
        public string RedirectUri { get; set; }

        public override void ExecuteResult(ControllerContext context)
        {
            var properties = new AuthenticationProperties { RedirectUri = RedirectUri };
            context.HttpContext.GetOwinContext().Authentication.Challenge(properties, LoginProvider);
        }
    }
}