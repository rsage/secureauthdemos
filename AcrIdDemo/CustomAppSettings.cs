﻿using System.Configuration;

namespace RefreshTokenDemo
{
    public static class CustomAppSettings
    {
        public static string TokenEndpointUrl
        {
            get { return ConfigurationManager.AppSettings["TokenEndpointUrl"]; }
        }

        public static string ClientId
        {
            get { return ConfigurationManager.AppSettings["ClientId"]; }
        }

        public static string ClientSecret
        {
            get { return ConfigurationManager.AppSettings["ClientSecret"]; }
        }

        public static string AuthenticationRedirectUrl
        {
            get { return ConfigurationManager.AppSettings["AuthenticationRedirectUrl"]; }
        }

        public static string ExternalServiceUrl
        {
            get { return ConfigurationManager.AppSettings["ExternalServiceUrl"]; }
        }
    }
}