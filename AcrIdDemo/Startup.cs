﻿using Microsoft.AspNet.Identity;
using Microsoft.IdentityModel.Protocols;
using Microsoft.Owin;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.Cookies;
using Microsoft.Owin.Security.OpenIdConnect;
using Owin;
using RefreshTokenDemo;
using System.IdentityModel.Claims;
using System.IdentityModel.Tokens;
using System.Security.Cryptography.X509Certificates;
using System.Web;

[assembly: OwinStartup(typeof(Startup))]

namespace RefreshTokenDemo
{
    public class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            app.UseErrorPage();

            var cookieAuthoptions = new CookieAuthenticationOptions
            {
                AuthenticationMode = AuthenticationMode.Active,
                AuthenticationType = DefaultAuthenticationTypes.ApplicationCookie
            };
            app.UseCookieAuthentication(cookieAuthoptions);
            //app.UseExternalSignInCookie(DefaultAuthenticationTypes.ExternalCookie);

            var oidcConfiguration = new OpenIdConnectConfiguration
            {
                AuthorizationEndpoint = "https://SecureAuth01VM.acr.org/secureauth7/authorized/oidcauthorize.aspx",
                Issuer = "https://SecureAuth01VM.acr.org/SecureAuth7",
                TokenEndpoint = CustomAppSettings.TokenEndpointUrl,
                UserInfoEndpoint = "https://SecureAuth01VM.acr.org/secureauth7/oidcuserinfo.aspx",
            };
            oidcConfiguration.ResponseTypesSupported.Add("id_token token");
            var certificate = HttpContext.Current.Server.MapPath("~/AcrConnect.cer");
            var cert = new X509Certificate2(certificate);
            oidcConfiguration.SigningKeys.Add(new X509AsymmetricSecurityKey(cert));

            var oidcOptions = new OpenIdConnectAuthenticationOptions
            {
                AuthenticationType = "ACR ID",
                Authority = "https://SecureAuth01VM.acr.org/secureauth7/",
                Caption = "ACR Connect",
                ClientId = CustomAppSettings.ClientId,
                ClientSecret = CustomAppSettings.ClientSecret,
                RedirectUri = CustomAppSettings.AuthenticationRedirectUrl,
                ResponseType = "id_token token",
                Scope = "openid profile",
                ResponseMode = OpenIdConnectResponseModes.Query,
                Configuration = oidcConfiguration,
                TokenValidationParameters =
                {
                    IssuerSigningKey = new X509AsymmetricSecurityKey(cert), 
                    SaveSigninToken = false,
                    AuthenticationType = DefaultAuthenticationTypes.ApplicationCookie,
                    NameClaimType = ClaimTypes.Email
                },
                ProtocolValidator = {RequireNonce = false},
                UseTokenLifetime = false,
                SignInAsAuthenticationType = DefaultAuthenticationTypes.ApplicationCookie
            };

            app.UseOpenIdConnectAuthentication(oidcOptions);
        }
    }
}