﻿using Microsoft.AspNet.Identity;
using Microsoft.IdentityModel.Protocols;
using Microsoft.Owin;
using Microsoft.Owin.Security.OpenIdConnect;
using Owin;
using RefreshTokenDemo;
using System.IdentityModel.Tokens;
using System.Security.Cryptography.X509Certificates;
using System.Web;

[assembly: OwinStartup(typeof(Startup))]

namespace RefreshTokenDemo
{
    public class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            app.UseErrorPage();

            app.UseExternalSignInCookie(DefaultAuthenticationTypes.ExternalCookie);

            var oidcConfiguration = new OpenIdConnectConfiguration
            {
                AuthorizationEndpoint = "https://SecureAuth01VM.acr.org/secureauth5/authorized/oidcauthorize.aspx",
                Issuer = "https://SecureAuth01VM.acr.org/SecureAuth5",
                TokenEndpoint = CustomAppSettings.TokenEndpointUrl,
                UserInfoEndpoint = "https://SecureAuth01VM.acr.org/secureauth5/oidcuserinfo.aspx",
            };
            oidcConfiguration.ResponseTypesSupported.Add("code");
            var certificate = HttpContext.Current.Server.MapPath("~/AcrConnect.cer");
            var cert = new X509Certificate2(certificate);
            oidcConfiguration.SigningKeys.Add(new X509AsymmetricSecurityKey(cert));

            var oidcOptions = new OpenIdConnectAuthenticationOptions
            {
                AuthenticationType = "ACR ID",
                Authority = "https://SecureAuth01VM.acr.org/secureauth5/",
                Caption = "ACR Connect",
                ClientId = CustomAppSettings.ClientId,
                ClientSecret = CustomAppSettings.ClientSecret,
                RedirectUri = CustomAppSettings.AuthenticationRedirectUrl,
                ResponseType = "code",
                Scope = "openid pqrs_data_submission grid_exam_submission",
                ResponseMode = OpenIdConnectResponseModes.Query,
                Configuration = oidcConfiguration,
                TokenValidationParameters =
                {
                    IssuerSigningKey = new X509AsymmetricSecurityKey(cert), 
                    SaveSigninToken = false,
                    AuthenticationType = DefaultAuthenticationTypes.ExternalCookie
                },
                ProtocolValidator = {RequireNonce = false},
                UseTokenLifetime = false,
                SignInAsAuthenticationType = DefaultAuthenticationTypes.ExternalCookie
            };

            app.UseOpenIdConnectAuthentication(oidcOptions);
        }
    }
}