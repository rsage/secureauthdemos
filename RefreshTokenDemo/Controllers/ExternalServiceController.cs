﻿using System;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web.Mvc;
using Thinktecture.IdentityModel.Client;

namespace RefreshTokenDemo.Controllers
{
    public class ExternalServiceController : Controller
    {
        // GET: ExternalService
        public async Task<ActionResult> CallService()
        {
            await CallExternalServiceAsync(SecureTokenStorage.AccessToken);

            return Redirect("/");
        }

        public async Task<ActionResult> GetAccessToken()
        {
            var oidcClient = new OAuth2Client(new Uri(CustomAppSettings.TokenEndpointUrl, UriKind.Absolute), CustomAppSettings.ClientId, CustomAppSettings.ClientSecret, OAuth2Client.ClientAuthenticationStyle.PostValues);
            var response = await oidcClient.RequestRefreshTokenAsync(SecureTokenStorage.RefreshToken);

            SecureTokenStorage.RefreshToken = response.RefreshToken;
            SecureTokenStorage.AccessToken = response.AccessToken;

            return Redirect("/");
        }
        

        private static async Task CallExternalServiceAsync(string accessToken)
        {
            try
            {
                var client = new HttpClient();
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", accessToken);
                var result = await client.GetStringAsync(CustomAppSettings.ExternalServiceUrl);
                SecureTokenStorage.ExternalServiceCallResult = result;
            }
            catch (Exception e)
            {
                SecureTokenStorage.ExternalServiceCallResult = e.Message;
            }
        }
    }
}