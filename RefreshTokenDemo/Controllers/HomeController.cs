﻿using System.Web.Mvc;

namespace RefreshTokenDemo.Controllers
{
    public class HomeController : Controller
    {
        // GET: Home
        public ActionResult Index()
        {
            return View();
        }
    }
}