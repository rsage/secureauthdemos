﻿using System;
using Microsoft.Owin.Security;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Thinktecture.IdentityModel.Client;

namespace RefreshTokenDemo.Controllers
{
    public class AuthenticationController : Controller
    {
        public ActionResult AcrConnectLogin()
        {
            return new ChallengeResult("ACR ID", "/Home");
        }

        public async Task<ActionResult> AcrConnectCallback(string code)
        {
            var oidcClient = new OAuth2Client(new Uri(CustomAppSettings.TokenEndpointUrl, UriKind.Absolute), CustomAppSettings.ClientId, CustomAppSettings.ClientSecret, OAuth2Client.ClientAuthenticationStyle.PostValues);
            var response = await oidcClient.RequestAuthorizationCodeAsync(code, CustomAppSettings.AuthenticationRedirectUrl);

            SecureTokenStorage.RefreshToken = response.RefreshToken;

            return new RedirectResult("/");
        }
    }

    internal class ChallengeResult : HttpUnauthorizedResult
    {
        public ChallengeResult(string provider, string redirectUri)
        {
            LoginProvider = provider;
            RedirectUri = redirectUri;
        }

        public string LoginProvider { get; set; }
        public string RedirectUri { get; set; }

        public override void ExecuteResult(ControllerContext context)
        {
            var properties = new AuthenticationProperties { RedirectUri = RedirectUri };
            context.HttpContext.GetOwinContext().Authentication.Challenge(properties, LoginProvider);
        }
    }
}