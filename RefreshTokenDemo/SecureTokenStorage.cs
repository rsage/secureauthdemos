﻿using System.Collections.Concurrent;

namespace RefreshTokenDemo
{
    public static class SecureTokenStorage
    {
        private static readonly ConcurrentDictionary<string, string> Data = new ConcurrentDictionary<string, string>();

        public static string RefreshToken
        {
            get
            {
                string val;
                return !Data.TryGetValue("refresh", out val) ? null : val;
            }

            set
            {
                Data.AddOrUpdate("refresh", value, (k, v) => value);
            }
        }

        public static string AccessToken
        {
            get
            {
                string val;
                return !Data.TryGetValue("AccessToken", out val) ? null : val;
            }

            set
            {
                Data.AddOrUpdate("AccessToken", value, (k, v) => value);
            }
        }

        public static string ExternalServiceCallResult
        {
            get
            {
                string val;
                return !Data.TryGetValue("external", out val) ? null : val;
            }

            set
            {
                Data.AddOrUpdate("external", value, (k, v) => value);
            }
        }
    }
}